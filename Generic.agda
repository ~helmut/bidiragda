module Generic where

import Category.Functor
import Category.Monad
open import Data.List using (List ; length) renaming ([] to []L ; _∷_ to _∷L_)
open import Data.Maybe using (Maybe ; just ; nothing)
import Data.Maybe.Categorical
open import Data.Maybe.Relation.Binary.Pointwise using (just ; nothing) renaming (setoid to MaybeEq)
open import Data.Nat using (ℕ ; zero ; suc)
open import Data.Product using (_×_ ; _,_)
open import Data.Vec using (Vec ; toList ; fromList ; map) renaming ([] to []V ; _∷_ to _∷V_)
import Data.Vec.Relation.Binary.Pointwise.Inductive as VecEq
open import Data.Vec.Properties using (map-cong)
open import Function using (_∘_ ; id ; flip)
open import Function.Equality using (_⟶_)
open import Level using () renaming (zero to ℓ₀)
open import Relation.Binary using (Setoid ; module Setoid)
open import Relation.Binary.Indexed.Heterogeneous using () renaming (IndexedSetoid to ISetoid)
open import Relation.Binary.PropositionalEquality as P using (_≡_ ; _≗_)

open Setoid using () renaming (_≈_ to _∋_≈_)
open Category.Functor.RawFunctor {Level.zero} Data.Maybe.Categorical.functor using (_<$>_)
open Category.Monad.RawMonad {Level.zero} Data.Maybe.Categorical.monad using (_>>=_)

≡-to-Π : {A B : Set} → (A → B) → P.setoid A ⟶ P.setoid B
≡-to-Π f = record { _⟨$⟩_ = f; cong = P.cong f }

sequenceV : {A : Set} {n : ℕ} → Vec (Maybe A) n → Maybe (Vec A n)
sequenceV []V       = just []V
sequenceV (x ∷V xs) = x >>= (λ y → (_∷V_ y) <$> sequenceV xs)

mapMV : {A B : Set} {n : ℕ} → (A → Maybe B) → Vec A n → Maybe (Vec B n)
mapMV f = sequenceV ∘ map f

mapMV-cong : {A B : Set} {f g : A → Maybe B} → f ≗ g → {n : ℕ} → mapMV {n = n} f ≗ mapMV g
mapMV-cong f≗g v = P.cong sequenceV (map-cong f≗g v)

mapMV-purity : {A B : Set} {n : ℕ} → (f : A → B) → (v : Vec A n) → mapMV (Maybe.just ∘ f) v ≡ just (map f v)
mapMV-purity f []V       = P.refl
mapMV-purity f (x ∷V xs) = P.cong (_<$>_ (_∷V_ (f x))) (mapMV-purity f xs)

maybeEq-from-≡ : {A : Set} {a b : Maybe A} → a ≡ b → MaybeEq (P.setoid A) ∋ a ≈ b
maybeEq-from-≡ {a = just x}  {b = .(just x)} P.refl = just P.refl
maybeEq-from-≡ {a = nothing} {b = .nothing}  P.refl = nothing

maybeEq-to-≡ : {A : Set} {a b : Maybe A} → MaybeEq (P.setoid A) ∋ a ≈ b → a ≡ b
maybeEq-to-≡ (just P.refl) = P.refl
maybeEq-to-≡ nothing       = P.refl

subst-cong : {A : Set} → (T : A → Set) → {g : A → A} → {a b : A} → (f : {c : A} → T c → T (g c)) → (p : a ≡ b) →
             f ∘ P.subst T p ≗ P.subst T (P.cong g p) ∘ f
subst-cong T f P.refl _ = P.refl

subst-fromList : {A : Set} {x y : List A} → (p : y ≡ x) →
                 P.subst (Vec A) (P.cong length p) (fromList y) ≡ fromList x
subst-fromList P.refl = P.refl

subst-subst : {A : Set} (T : A → Set) {a b c : A} → (p : a ≡ b) → (p′ : b ≡ c) → (x : T a) →
              P.subst T p′ (P.subst T p x) ≡ P.subst T (P.trans p p′) x
subst-subst T P.refl p′ x = P.refl

toList-subst : {A : Set} → {n m : ℕ} (v : Vec A n) → (p : n ≡ m) →
               toList (P.subst (Vec A) p v) ≡ toList v
toList-subst v P.refl = P.refl

VecISetoid : Setoid ℓ₀ ℓ₀ → ISetoid ℕ ℓ₀ ℓ₀
VecISetoid S = record
  { Carrier = Vec (Setoid.Carrier S)
  ; _≈_ = VecEq.Pointwise (Setoid._≈_ S)
  ; isEquivalence = record
    { refl = VecEq.refl (Setoid.refl S)
    ; sym = VecEq.sym (Setoid.sym S)
    ; trans = VecEq.trans (Setoid.trans S) }
  }
